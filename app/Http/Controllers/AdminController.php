<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Loan;
use App\Submission;
use Validator;
use Auth;
use Flash;

class AdminController extends Controller
{
    public function showLoans(){
        if(Auth::user()->role == 'user'){
            return redirect()->route('home');
        }
        $loans = Loan::paginate(5);
        return view('admin/showLoans')->with('loans', $loans);
    }

    public function addLoan(){
        if(Auth::user()->role == 'user'){
            return redirect()->route('home');
        }
        return view('admin/addLoan');
    }

    public function saveLoan(Request $request) {
        $loans = Loan::paginate(5);
        if(Auth::user()->role == 'user'){
            return redirect()->route('home');
        }
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'purpose' => 'required',
            'eligibility' => 'required',
            'interest' => 'required',
            'tenure' => 'required',
            'repayment' => 'required',
            'documents' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()->back()
            ->withInput($request->all())
            ->withErrors($validator->errors());
        }
        $data = $request->all();
        
        $submission = Loan::create($data);
        Flash::success(trans('New loan created successfully!'));
        return redirect()->route('showAdminLoans')->with('loans', $loans);
    }

    public function viewLoan(Loan $loan){
        if(Auth::user()->role == 'user'){
            return redirect()->route('home');
        }
        return view('admin/viewLoan')->with('loan', $loan);
    }

    public function editLoan(Loan $loan){
        if(Auth::user()->role == 'user'){
            return redirect()->route('home');
        }
        return view('admin/editLoan')->with('loan', $loan);
    }

    public function updateLoan(Request $request) {
        if(Auth::user()->role == 'user'){
            return redirect()->route('home');
        }
        $loan = Loan::find($request->id);
        $loans = Loan::paginate(5);
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'purpose' => 'required',
            'eligibility' => 'required',
            'interest' => 'required',
            'tenure' => 'required',
            'repayment' => 'required',
            'documents' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()->back()
            ->withInput($request->all())
            ->withErrors($validator->errors());
        }
        $data = $request->all();
        
        $loan->update($data);
        Flash::success(trans('Loan updated successfully!'));
        return redirect()->route('showAdminLoans')->with('loans', $loans);
    }

    public function deleteLoan(Loan $loan){
        if(Auth::user()->role == 'user'){
            return redirect()->route('home');
        }
        $loans = Loan::paginate(5);
        if($loan->submissions()->count() > 0){
            Flash::warning(trans('Warning! Loan cannot be deleted as there are loan applications assigned to it!'));
            return redirect()->route('showAdminLoans')->with('loans', $loans);
        }
        $loan->delete();
        Flash::success(trans('Loan deleted successfully!'));
        
    }

    public function showSubmissions(){
        if(Auth::user()->role == 'user'){
            return redirect()->route('home');
        }
        $submissions = Submission::paginate(5);
        return view('admin/showSubmissions')->with('submissions', $submissions);
    }

    public function viewSubmission(Submission $submission){
        if(Auth::user()->role == 'user'){
            return redirect()->route('home');
        }
        return view('admin/viewSubmission')->with('submission', $submission);
    }

    public function approveSubmission(Request $request, Submission $submission){
        if(Auth::user()->role == 'user'){
            return redirect()->route('home');
        }
        $data['status'] = $request->status;
        $submission->update($data);
        Flash::success(trans('Loan application approved successfully!'));
        return redirect()->back();
    }
    public function rejectSubmission(Request $request, Submission $submission){
        if(Auth::user()->role == 'user'){
            return redirect()->route('home');
        }
        $data['status'] = $request->status;
        $data['reason'] = $request->reason;
        $submission->update($data);
        Flash::success(trans('Loan application rejected successfully!'));
        return redirect()->back();
    }
}

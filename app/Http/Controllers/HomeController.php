<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Loan;
use App\Submission;
use Validator;
use Auth;
use Flash;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    public function welcome()
    {
        $loans = Loan::paginate(10);
        return view('welcome')->with('loans', $loans);
    }

    public function about()
    {
        return view('about');
    }

    public function contact()
    {
        return view('contact');
    }

    public function viewLoan(Loan $loan)
    {
        return view('viewLoan')->with('loan', $loan);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function dashboard()
    {
        if(Auth::user()){
            if(Auth::user()->role == 'admin'){
                $loan_count = Loan::all()->count();
                $recent = Submission::where('status','pending')->orderBy('created_at','desc')->take(5)->get();
                $pending = Submission::where('status','pending')->get()->count();
                $approved = Submission::where('status','approved')->get()->count();
                $rejected = Submission::where('status','rejected')->get()->count();
                return view('admin')->with(['loans' => $loan_count, 
                                            'pending' => $pending, 
                                            'approved' => $approved, 
                                            'rejected' => $rejected,
                                            'recent' => $recent]);
            }
            else {
                $loans = Loan::paginate(10);
                return view('welcome')->with('loans', $loans);
            }
        }
        return view('home');
    }

    public function showSubmissions(){
        $submissions = Auth::user()->submissions()->get();
        return view('showSubmissions')->with('submissions', $submissions);
    }

    public function viewSubmission(Submission $submission){
        return view('viewSubmission')->with('submission', $submission);
    }

    public function editSubmission(Submission $submission){
        return view('editSubmission')->with('submission', $submission);
    }

    public function createSubmission(){
        return view('createSubmission');
    }
    public function storeSubmission(Request $request) {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'phone1' => 'required',
            'home_address' => 'required',
            'ssn' => 'required',
            'passport_number' => 'required',
            'request_amount' => 'required|numeric',
            'maturity' => 'required',
            'bank_statement' => 'required',
            'it_returns' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()->back()
            ->withInput($request->all())
            ->withErrors($validator->errors());
        }
        $data = $request->all();
        $data['user_id'] = Auth::user()->id;

        if ($request->hasFile('bank_statement')) {
            $bank_statment = $request->file('bank_statement');
            $strippedName = str_replace(' ', '', $bank_statment->getClientOriginalName());
            $bankStatementName = date('Y-m-d-H-i-s').$strippedName;
            $request->file('bank_statement')->storeAs('documents', $bankStatementName);
            $data['bank_statement'] = $bankStatementName;
        }
        if ($request->hasFile('it_returns')) {
            $it_returns = $request->file('it_returns');
            $strippedName = str_replace(' ', '', $it_returns->getClientOriginalName());
            $itReturnsName = date('Y-m-d-H-i-s').$strippedName;
            $request->file('it_returns')->storeAs('documents', $itReturnsName);
            $data['it_returns'] = $itReturnsName;
        }
        if ($request->hasFile('address_proof')) {
            $address_proof = $request->file('address_proof');
            $strippedName = str_replace(' ', '', $address_proof->getClientOriginalName());
            $addressProofName = date('Y-m-d-H-i-s').$strippedName;
            $request->file('address_proof')->storeAs('documents', $addressProofName);
            $data['address_proof'] = $addressProofName;
        }
        $submission = Submission::create($data);
        Flash::success(trans('Loan Application Submitted'));
        $submissions = Auth::user()->submissions()->get();
        return view('showSubmissions')->with('submissions', $submissions);
    }

    public function updateSubmission(Request $request) {
        
        $submission = Submission::find($request->id);

        $validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'phone1' => 'required',
            'home_address' => 'required',
            'ssn' => 'required',
            'passport_number' => 'required',
            'request_amount' => 'required',
            'maturity' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()->back()
            ->withInput($request->all())
            ->withErrors($validator->errors());
        }
        $data = $request->all();
        $data['user_id'] = Auth::user()->id;

        if ($request->hasFile('bank_statement')) {
            $bank_statment = $request->file('bank_statement');
            $strippedName = str_replace(' ', '', $bank_statment->getClientOriginalName());
            $bankStatementName = date('Y-m-d-H-i-s').$strippedName;
            $request->file('bank_statement')->storeAs('documents', $bankStatementName);
            $data['bank_statement'] = $bankStatementName;
        }
        if ($request->hasFile('it_returns')) {
            $it_returns = $request->file('it_returns');
            $strippedName = str_replace(' ', '', $it_returns->getClientOriginalName());
            $itReturnsName = date('Y-m-d-H-i-s').$strippedName;
            $request->file('it_returns')->storeAs('documents', $itReturnsName);
            $data['it_returns'] = $itReturnsName;
        }
        if ($request->hasFile('address_proof')) {
            $address_proof = $request->file('address_proof');
            $strippedName = str_replace(' ', '', $address_proof->getClientOriginalName());
            $addressProofName = date('Y-m-d-H-i-s').$strippedName;
            $request->file('address_proof')->storeAs('documents', $addressProofName);
            $data['address_proof'] = $addressProofName;
        }
        $data['loan_id'] = $request->loan_id;
        $data['status'] = 'pending';
        $submission->update($data);
        Flash::success(trans('Loan Application Updated'));

        $submissions = Auth::user()->submissions()->get();
        return view('showSubmissions')->with('submissions', $submissions);
    }

    public function deleteSubmission(Submission $submission){
        $submission->delete();
        Flash::success(trans('Submission deleted successfully!'));
        
        $submissions = Auth::user()->submissions()->get();
        return view('showSubmissions')->with('submissions', $submissions);
    }
}

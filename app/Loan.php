<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Loan extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'eligibility','purpose','interest','tenure','repayment','documents'
    ];

    public function submissions()
    {
        return $this->hasMany('App\Submission');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Submission extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'loan_id', 'first_name','last_name','ssn',
        'date_of_birth','phone1','home_address','passport_number',
        'drivers_license','job_type','job_name','job_address',
        'salary','request_amount','maturity','capacity','status',
        'address_proof','it_returns','bank_statement','reason'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function loan()
    {
        return $this->belongsTo('App\Loan');
    }
}

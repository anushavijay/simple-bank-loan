<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Submissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('submissions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id')->unsigned()->references('id')->on('users');
            $table->integer('loan_id')->unsigned()->references('id')->on('loans');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('ssn');
            $table->string('date_of_birth')->nullable();
            $table->string('phone1');
            $table->text('home_address');
            $table->string('passport_number');
            $table->string('drivers_license')->nullable();
            $table->string('job_name')->nullable();
            $table->text('job_address')->nullable();
            $table->string('salary')->nullable();
            $table->string('request_amount');
            $table->string('maturity');
            $table->string('capacity')->nullable();;
            $table->string('status')->default('pending');
            $table->string('address_proof')->nullable();
            $table->string('it_returns');
            $table->string('bank_statement');
            $table->text('reason')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('submissions', function (Blueprint $table) {
            $table->dropForeign('submissions_user_id_foreign');
            $table->dropForeign('submissions_loan_id_foreign');
        });
    }
}

<?php

use Illuminate\Database\Seeder;
use App\Loan;

class LoansTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        
        $loan = Loan::firstOrNew(['id' => 1]);
        $loan->name = 'Vehicle Loan';
        $loan->eligibility = $faker->paragraph($nb = 10, $asText = false);
        $loan->purpose = $faker->paragraph($nb = 10, $asText = false);
        $loan->interest = '10';
        $loan->tenure = '5 years';
        $loan->repayment = $faker->paragraph($nb = 10, $asText = false);
        $loan->documents = $faker->paragraph($nb = 10, $asText = false);
        $loan->save();

        $loan = Loan::firstOrNew(['id' => 2]);
        $loan->name = 'Personal Loan';
        $loan->eligibility = $faker->paragraph($nb = 10, $asText = false);
        $loan->purpose = $faker->paragraph($nb = 10, $asText = false);
        $loan->interest = '12';
        $loan->tenure = '3 years';
        $loan->repayment = $faker->paragraph($nb = 10, $asText = false);
        $loan->documents = $faker->paragraph($nb = 10, $asText = false);
        $loan->save();

        $loan = Loan::firstOrNew(['id' => 3]);
        $loan->name = 'House Loan';
        $loan->eligibility = $faker->paragraph($nb = 10, $asText = false);
        $loan->purpose = $faker->paragraph($nb = 10, $asText = false);
        $loan->interest = '8';
        $loan->tenure = '15 years';
        $loan->repayment = $faker->paragraph($nb = 10, $asText = false);
        $loan->documents = $faker->paragraph($nb = 10, $asText = false);
        $loan->save();

        $loan = Loan::firstOrNew(['id' => 4]);
        $loan->name = 'Education Loan';
        $loan->eligibility = $faker->paragraph($nb = 10, $asText = false);
        $loan->purpose = $faker->paragraph($nb = 10, $asText = false);
        $loan->interest = '6';
        $loan->tenure = '2 years';
        $loan->repayment = $faker->paragraph($nb = 10, $asText = false);
        $loan->documents = $faker->paragraph($nb = 10, $asText = false);
        $loan->save();

        $loan = Loan::firstOrNew(['id' => 5]);
        $loan->name = 'Two wheeler Loan';
        $loan->eligibility = $faker->paragraph($nb = 10, $asText = false);
        $loan->purpose = $faker->paragraph($nb = 10, $asText = false);
        $loan->interest = '9';
        $loan->tenure = '5 years';
        $loan->repayment = $faker->paragraph($nb = 10, $asText = false);
        $loan->documents = $faker->paragraph($nb = 10, $asText = false);
        $loan->save();

        $loan = Loan::firstOrNew(['id' => 6]);
        $loan->name = 'Agriculture Loan';
        $loan->eligibility = $faker->paragraph($nb = 10, $asText = false);
        $loan->purpose = $faker->paragraph($nb = 10, $asText = false);
        $loan->interest = '4';
        $loan->tenure = '10 years';
        $loan->repayment = $faker->paragraph($nb = 10, $asText = false);
        $loan->documents = $faker->paragraph($nb = 10, $asText = false);
        $loan->save();

        $loan = Loan::firstOrNew(['id' => 7]);
        $loan->name = 'Four wheeler Loan';
        $loan->eligibility = $faker->paragraph($nb = 10, $asText = false);
        $loan->purpose = $faker->paragraph($nb = 10, $asText = false);
        $loan->interest = '12';
        $loan->tenure = '3 years';
        $loan->repayment = $faker->paragraph($nb = 10, $asText = false);
        $loan->documents = $faker->paragraph($nb = 10, $asText = false);
        $loan->save();
    }
}

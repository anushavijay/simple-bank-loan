<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Create admin user
        $user = User::firstOrNew(['email' => 'admin@gmail.com']);
        $user->name = 'Admin';
        $user->email = 'admin@gmail.com';
        $user->role = 'admin';
        $user->email_verified_at = now();
        $user->password = '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi'; // password
        $user->remember_token = Str::random(10);
        $user->save();

        //Create admin user
        $user = User::firstOrNew(['email' => 'vinay@gmail.com']);
        $user->name = 'Vinay';
        $user->email = 'vinay@gmail.com';
        $user->role = 'user';
        $user->email_verified_at = now();
        $user->password = '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi'; // password
        $user->remember_token = Str::random(10);
        $user->save();

        //Populate dummy users
        factory(User::class, 40)->create();
    }
}

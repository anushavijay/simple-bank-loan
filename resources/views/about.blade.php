@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <span class="card-heading">About</span>
                </div>
                <div class="card-body">
                <h4>Our Company</h4>
                <p>Our relationships are built on trust that we build every day through every interaction. Our employees are empowered to do the right thing to ensure they share our customers’ vision for success. We work as a partner to provide financial products and services that make banking safe, simple and convenient. We’re here to help navigate important milestones and strengthen futures together.</p>
                <h4>Our History</h4>   
                <p>Our history dates back to 1863 when First National Bank of Cincinnati opened for business. As regional banks proliferated across the country in the years that followed, a number of other predecessors were born. After a series of mergers at the turn of the 21st century, we formally took on the U.S. Bank name and established our headquarters in Minnesota. Today, U.S. Bank is the fifth-largest bank in the country, with 74,000 employees and $467 billion in assets as of December 31, 2018.</p> 
                <h4>Our Businesses</h4>
                <p>
Our businesses
Our diverse business mix is fundamental in delivering a consistent, predictable and repeatable financial performance year after year. Our core businesses include Consumer & Business Banking, Corporate & Commercial Banking, Payment Services and Wealth Management & Investment Services. Through our “One U.S. Bank” philosophy, we are able to bring the power of the whole bank to every customer, every single day.</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

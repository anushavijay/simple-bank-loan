@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-3">
            <div class="card text-center">
                <div class="card-header">Total Loans</div>
                <div class="card-body">
                    <h1>{{ $loans }}</h1>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card text-center">
                <div class="card-header">Approved Applications</div>
                <div class="card-body">
                    <h1>{{ $approved }}</h1>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card text-center">
                <div class="card-header">Pending Applications</div>
                <div class="card-body">
                    <h1>{{ $pending }}</h1>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card text-center">
                <div class="card-header">Rejected Application</div>
                <div class="card-body">
                    <h1>{{ $rejected }}</h1>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Recent Pending Applications
                </div>
                <div class="card-body">
                @include('flash::message')  
                    @if(count($recent) > 0)
                        <table class="table table-hover">
                            <thead class="thead-light">
                                <tr>
                                <th scope="col">Name</th>
                                <th scope="col">User Name</th>
                                <th scope="col">User E-mail</th>
                                <th scope="col">Applied On</th>
                                <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($recent as $submission)    
                                <tr>
                                    <td>{{ $submission->loan->name }}</td>
                                    <td><i class="fas fa-user"></i> {{ $submission->user->name }}</td>
                                    <td><i class="fas fa-envelope"></i> {{ $submission->user->email }}</td>
                                    <td><i class="fas fa-calendar-alt"></i> {{ $submission->created_at }}</td>
                                    <td><a href="{{ url('/dashboard/submissions/'. $submission->id .'/view') }}" class="btn btn-primary btn-sm"><i class="fas fa-eye"></i> View Application</a></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        @else
                        <div class="alert alert-warning">
                            No pending loan applications are found!
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

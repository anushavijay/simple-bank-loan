@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <span class="card-heading">Add New Loan</span>
                </div>
                <div class="card-body">
                    @include('flash::message')  
                    <form method="POST" action="{{ route('saveLoan') }}" class="loan-form" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="">Name <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="name" value="{{ old('name') }}">
                                    @if ($errors->has('name'))
                                        <span class="help-block text-danger">
                                            * {{ $errors->first('name') }}
                                        </span>
                                    @endif   
                                </div>
                            </div>
                        </div><!-- /row -->
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Purpose <span class="text-danger">*</span></label>
                                    <textarea rows="5" class="form-control" name="purpose" value="{{ old('purpose') }}"></textarea>
                                    @if ($errors->has('purpose'))
                                        <span class="help-block text-danger">
                                            * {{ $errors->first('purpose') }}
                                        </span>
                                    @endif   
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Eligibility <span class="text-danger">*</span></label>
                                    <textarea rows="5" class="form-control" name="eligibility" value="{{ old('eligibility') }}"></textarea>
                                    @if ($errors->has('eligibility'))
                                        <span class="help-block text-danger">
                                            * {{ $errors->first('eligibility') }}
                                        </span>
                                    @endif   
                                </div>
                            </div>
                        </div><!-- /row -->
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Rate of Interest <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="interest" value="{{ old('interest') }}">
                                    @if ($errors->has('interest'))
                                        <span class="help-block text-danger">
                                            * {{ $errors->first('interest') }}
                                        </span>
                                    @endif   
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Tenure <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="tenure" value="{{ old('tenure') }}">
                                    @if ($errors->has('tenure'))
                                        <span class="help-block text-danger">
                                            * {{ $errors->first('tenure') }}
                                        </span>
                                    @endif   
                                </div>
                            </div>
                        </div><!-- /row -->
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Repayment <span class="text-danger">*</span></label>
                                    <textarea rows="5" class="form-control" name="repayment" value="{{ old('repayment') }}"></textarea>
                                    @if ($errors->has('repayment'))
                                        <span class="help-block text-danger">
                                            * {{ $errors->first('repayment') }}
                                        </span>
                                    @endif   
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Documents required <span class="text-danger">*</span></label>
                                    <textarea rows="5" class="form-control" name="documents" value="{{ old('documents') }}"></textarea>
                                    @if ($errors->has('documents'))
                                        <span class="help-block text-danger">
                                            * {{ $errors->first('documents') }}
                                        </span>
                                    @endif   
                                </div>
                            </div>
                        </div><!-- /row -->
                        <div class="row">
                            <div class="col-md-6">
                                <button type="reset" class="btn btn-info btn-block text-white text-uppercase">Reset Details</button>
                            </div>
                            <div class="col-md-6">
                                <button type="submit" class="btn btn-block btn-success text-uppercase">add new loan</button>
                            </div>        
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

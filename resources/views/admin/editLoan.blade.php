@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <span class="card-heading">Edit Loan: {{ $loan->name }}</span>
                </div>
                <div class="card-body">
                    @include('flash::message')  
                    <form method="POST" action="{{ url('/dashboard/loans/'. $loan->id .'/update') }}" class="loan-form" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="id" value="{{ $loan->id }}">
                        
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="">Name <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="name" value="{{ $loan->name }}">
                                    @if ($errors->has('name'))
                                        <span class="help-block text-danger">
                                            * {{ $errors->first('name') }}
                                        </span>
                                    @endif   
                                </div>
                            </div>
                        </div><!-- /row -->
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Purpose <span class="text-danger">*</span></label>
                                    <textarea rows="5" class="form-control" name="purpose">{{ $loan->purpose }}</textarea>
                                    @if ($errors->has('purpose'))
                                        <span class="help-block text-danger">
                                            * {{ $errors->first('purpose') }}
                                        </span>
                                    @endif   
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Eligibility <span class="text-danger">*</span></label>
                                    <textarea rows="5" class="form-control" name="eligibility">{{ $loan->eligibility }}</textarea>
                                    @if ($errors->has('eligibility'))
                                        <span class="help-block text-danger">
                                            * {{ $errors->first('eligibility') }}
                                        </span>
                                    @endif   
                                </div>
                            </div>
                        </div><!-- /row -->
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Rate of Interest <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="interest" value="{{ $loan->interest }}">
                                    @if ($errors->has('interest'))
                                        <span class="help-block text-danger">
                                            * {{ $errors->first('interest') }}
                                        </span>
                                    @endif   
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Tenure <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="tenure" value="{{ $loan->tenure }}">
                                    @if ($errors->has('tenure'))
                                        <span class="help-block text-danger">
                                            * {{ $errors->first('tenure') }}
                                        </span>
                                    @endif   
                                </div>
                            </div>
                        </div><!-- /row -->
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Repayment <span class="text-danger">*</span></label>
                                    <textarea rows="5" class="form-control" name="repayment">{{ $loan->repayment }}</textarea>
                                    @if ($errors->has('repayment'))
                                        <span class="help-block text-danger">
                                            * {{ $errors->first('repayment') }}
                                        </span>
                                    @endif   
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Documents required <span class="text-danger">*</span></label>
                                    <textarea rows="5" class="form-control" name="documents">{{ $loan->documents }}</textarea>
                                    @if ($errors->has('documents'))
                                        <span class="help-block text-danger">
                                            * {{ $errors->first('documents') }}
                                        </span>
                                    @endif   
                                </div>
                            </div>
                        </div><!-- /row -->
                        <div class="row">
                            <div class="col-md-6">
                                <button type="reset" class="btn btn-info btn-block text-white text-uppercase">Reset Details</button>
                            </div>
                            <div class="col-md-6">
                                <button type="submit" class="btn btn-block btn-success text-uppercase">Update Loan</button>
                            </div>        
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

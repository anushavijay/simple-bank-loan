@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <a href="{{ route('addLoan') }}" class="btn btn-primary float-right text-white"><i class="fas fa-plus"></i> Add New Loan</a>	
                    <span class="card-heading">Loans Listing</span>
                </div>
                <div class="card-body">
                    @include('flash::message')  
                    @if(count($loans) > 0)
                        <table class="table table-hover">
                            <thead class="thead-light">
                                <tr>
                                <th scope="col">Name</th>
                                <th scope="col">Interest</th>
                                <th scope="col">Tenure</th>
                                <th scope="col">Created On</th>
                                <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($loans as $loan)    
                                <tr>
                                <td>{{ $loan->name }}</td> 
                                <td>{{ $loan->interest }}%</td> 
                                <td>{{ $loan->tenure }}</td> 
                                <td><i class="fas fa-calendar-alt"></i> {{ $loan->created_at }}</td> 
                                <td>
                                    <ul class="list-inline list-unstyled">
                                        <li class="list-inline-item"><a href="{{ url('/dashboard/loans/'.$loan->id.'/view') }}" class="btn btn-success text-white btn-sm"><i class="fas fa-eye"></i></a></li>
                                        <li class="list-inline-item"><a href="{{ url('/dashboard/loans/'. $loan->id .'/edit') }}" class="btn btn-info text-white btn-sm"><i class="fas fa-edit"></i></a></li>
                                        <li class="list-inline-item"><a href="{{ url('/dashboard/loans/'. $loan->id .'/delete') }}" class="btn btn-danger btn-sm"><i class="fas fa-trash-alt"></i></a></li>
                                    </ul>
                                </td> 
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="text-center">
                        {{ $loans->links() }}
                        </div>
                        
                    @else
                        <div class="alert alert-warning">
                            Loans are empty!
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

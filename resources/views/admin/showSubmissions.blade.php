@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <span class="card-heading">Users Loan Applications</span>
                </div>
                <div class="card-body">
                    @include('flash::message')  
                    @if(count($submissions) > 0)
                        <table class="table">
                            <thead class="thead-light">
                                <tr>
                                <th scope="col">Loan Name</th>
                                <th scope="col">User Name</th>
                                <th scope="col">User E-mail</th>
                                <th scope="col">Status</th>
                                <th scope="col">Applied On</th>
                                <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($submissions as $submission)    
                                <tr>
                                <td>{{ $submission->loan->name }}</td> 
                                <td><i class="fas fa-user"></i> {{ $submission->user->name }}</td> 
                                <td><i class="fas fa-envelope"></i> {{ $submission->user->email }}</td> 
                                <td>
                                    @if($submission->status == 'pending')
                                        <span class="badge badge-primary">
                                    @elseif($submission->status == 'approved')
                                        <span class="badge badge-success">
                                    @elseif($submission->status == 'rejected')
                                        <span class="badge badge-danger">
                                    @endif
                                     {{ $submission->status }}
                                    </span>
                                </td> 
                                <td><i class="fas fa-calendar-alt"></i> {{ $submission->created_at }}</td> 
                                <td>
                                    <a href="{{ url('/dashboard/submissions/'. $submission->id .'/view') }}" class="btn btn-primary btn-sm"><i class="fas fa-eye"></i> View Application</a>
                                </td> 
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="text-center">
                        {{ $submissions->links() }}
                        </div>
                        
                    @else
                        <div class="alert alert-warning">
                            Loan Applications are empty!
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

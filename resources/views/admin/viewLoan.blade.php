@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card view-loan">
                <div class="card-header">
                    <span class="card-heading">{{ $loan->name }}</span>
                    <ul class="list-inline list-unstyled">
                        <li class="list-inline-item"><a href="{{ route('addLoan') }}" class="btn btn-success text-white btn-sm"><i class="fas fa-plus"></i> Add New Loan</a></li>    
                        <li class="list-inline-item"><a href="{{ url('/dashboard/loans/'. $loan->id .'/edit') }}" class="btn btn-primary text-white btn-sm"><i class="fas fa-edit"></i> Edit Loan</a></li>
                        <li class="list-inline-item"><a href="{{ url('/dashboard/loans/'. $loan->id .'/delete') }}" class="btn btn-danger btn-sm"><i class="fas fa-trash-alt"></i> Delete Loan</a></li>
                    </ul>
                </div>
                <div class="card-body">
                    @include('flash::message')  
                    <h4>Purpose</h4>
                    <p>{{ $loan->purpose }}</p>
                    <h4>Eligibility</h4>
                    <p>{{ $loan->eligibility }}</p>
                    <h4>Repayment</h4>
                    <p>{{ $loan->repayment }}</p>
                    <h4>Dcouments Required</h4>
                    <p>{{ $loan->documents }}</p>
                    <p><b>Rate of Interest:</b> {{ $loan->interest }}</p>
                    <p><b>Tenure for loan:</b> {{ $loan->tenure }}</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

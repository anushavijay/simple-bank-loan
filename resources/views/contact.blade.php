@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <span class="card-heading">Contact</span>
                </div>
                <div class="card-body">
                <div class="row">
                    <div class="col-md-4">
                        <p>Please feel free to call us at 1-800-800-8888 with any questions or concerns. 
                        </p>
                        <h5>Customer care hours</h5>
                        <table class="table table-bordered">
                            <tbody>
                                <tr>
                                    <td>Monday</td>
                                    <td>8:00am - 6:00pm</td>
                                </tr>
                                <tr>
                                    <td>Tuesday</td>
                                    <td>8:00am - 6:00pm</td>
                                </tr>
                                <tr>
                                    <td>Wednesday</td>
                                    <td>8:00am - 6:00pm</td>
                                </tr>
                                <tr>
                                    <td>Thursday</td>
                                    <td>8:00am - 7:00pm</td>
                                </tr>
                                <tr>
                                    <td>Friday</td>
                                    <td>8:00am - 8:00pm</td>
                                </tr>
                                <tr>
                                    <td>Saturday</td>
                                    <td>8:00am - 2:00pm</td>
                                </tr>
                                <tr>
                                    <td>Sunday</td>
                                    <td>10:00am - 2:00pm</td>
                                </tr>
                            </tbody>
                        </table>
                        
                    </div>
                    <div class="col-md-8">
                        <form action="#">
                            <div class="form-group">
                                <label for="">Name</label>
                                <input type="text" class="form-control" placeholder="Enter your name">
                            </div>
                            <div class="form-group">
                                <label for="">Email</label>
                                <input type="text" class="form-control" placeholder="Add your genuine e-mail address">
                            </div>
                            <div class="form-group">
                                <label for="">Main concern</label>
                                <select class="form-control" id="">
                                    <option value="1">ATM / Debit card</option>
                                    <option value="2">eStatements</option>
                                    <option value="3">IRA</option>
                                    <option value="4">Loans</option>
                                    <option value="5">Online banking</option>
                                    <option value="6">Business banking</option>
                                    <option value="7">Suggestions</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">Message</label>
                                <textarea class="form-control" id="" cols="30" rows="5"></textarea>
                            </div>
                            <button class="btn btn-primary">Submit Request</button>
                        </form>
                    </div>
</div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

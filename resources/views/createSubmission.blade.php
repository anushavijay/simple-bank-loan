@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <h2>Create Loan Application</h2>
            @include('flash::message')  
            <form method="POST" action="{{ route('storeSubmission') }}" class="loan-form" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="card">
                    <div class="card-header">Personal Details</div>
                    <div class="card-body">
                        <input type="hidden" name="loan_id" value = "{{ Request::segment(2) }}">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">First Name <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="first_name" value="{{ old('first_name') }}">
                                    @if ($errors->has('first_name'))
                                        <span class="help-block text-danger">
                                            * {{ $errors->first('first_name') }}
                                        </span>
                                    @endif   
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Last Name <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="last_name" value="{{ old('last_name') }}">
                                    @if ($errors->has('last_name'))
                                        <span class="help-block text-danger">
                                            * {{ $errors->first('last_name') }}
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div><!-- /row -->
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Date of birth</label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1"><i class="fas fa-calendar-alt"></i></span>
                                        </div>
                                        <!-- <input class="datepicker" data-date-format="mm/dd/yyyy"> -->
                                        <input type="text" class="form-control" name="date_of_birth" placeholder="mm / dd / yy" value="{{ old('date_of_birth') }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="">Contact number <span class="text-danger">*</span></label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1"><i class="fas fa-phone-alt"></i></span>
                                        </div>
                                        <input type="text" class="form-control" name="phone1" value="{{ old('phone1') }}">
                                    </div>
                                    @if ($errors->has('phone1'))
                                        <span class="help-block text-danger">
                                            * Contact number is mandatory
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Home address <span class="text-danger">*</span></label>
                                    <textarea name="home_address" class="form-control" cols="20" rows="5" value="{{ old('home_address') }}"></textarea>
                                </div>
                            </div>
                        </div><!-- /row -->
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">Identity Details</div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="">Social Security Number(SSN) <span class="text-danger">*</span></label>
                                    <input type="text" name="ssn" class="form-control" id="" value="{{ old('ssn') }}">
                                    @if ($errors->has('ssn'))
                                        <span class="help-block text-danger">
                                            * {{ $errors->first('ssn') }}
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="">Passport number <span class="text-danger">*</span></label>
                                    <input type="text" name="passport_number" class="form-control" id="" value="{{ old('passport_number') }}">
                                    @if ($errors->has('passport_number'))
                                        <span class="help-block text-danger">
                                            * {{ $errors->first('passport_number') }}
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="">Driver's license</label>
                                    <input type="text" name="drivers_license" class="form-control" id="" value="{{ old('drivers_license') }}">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">Profession Details</div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Company name</label>
                                    <input type="text" name="job_name" class="form-control" value="{{ old('job_name') }}">
                                </div> 
                                <div class="form-group">
                                    <label for="">Net income </label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1"><i class="fas fa-dollar-sign"></i></span>
                                        </div>
                                        <input type="text" class="form-control" name="salary" value="{{ old('salary') }}">
                                    </div>
                                </div>   
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Company address</label>
                                    <textarea name="job_address" class="form-control" cols="30" rows="5" value="{{ old('job_address') }}"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">Loan Details</div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="">Requested amount <span class="text-danger">*</span></label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1"><i class="fas fa-dollar-sign"></i></span>
                                        </div>
                                        <input type="text" class="form-control" name="request_amount" value="{{ old('request_amount') }}">
                                    </div>
                                    @if ($errors->has('request_amount'))
                                        <span class="help-block text-danger">
                                            * Loan amount is mandatory
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="">Maturity <span class="text-danger">*</span></label>
                                    <input type="text" name="maturity" class="form-control" id="" value="{{ old('maturity') }}">
                                    @if ($errors->has('maturity'))
                                        <span class="help-block text-danger">
                                            * Maturity is mandatory
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="">Capacity</label>
                                    <input type="text" name="capacity" class="form-control" id="" value="{{ old('capacity') }}">
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- /card -->
                <div class="card">
                    <div class="card-header">Supporting Documents</div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="">Bank Statement <span class="text-danger">*</span></label>
                                    <input type="file" name="bank_statement" id="" value="{{ old('bank_statement') }}"><br>
                                    @if ($errors->has('bank_statement'))
                                        <span class="help-block text-danger">
                                            * Attach your bank statement
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="">IT Returns <span class="text-danger">*</span></label>
                                    <input type="file" name="it_returns" id="" value="{{ old('it_returns') }}"><br>
                                    @if ($errors->has('it_returns'))
                                        <span class="help-block text-danger">
                                            * Attach your IT returns
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="">Address proof </label>
                                    <input type="file" name="address_proof" id="" value="{{ old('address_proof') }}">
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- /card -->
                <div class="row">
                    <div class="col-md-6">
                        <button type="reset" class="btn btn-info btn-block text-white text-uppercase">Reset Details</button>
                    </div>
                    <div class="col-md-6">
                        <button type="submit" class="btn btn-block btn-success text-uppercase">Submit Loan Application</button>
                    </div>        
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

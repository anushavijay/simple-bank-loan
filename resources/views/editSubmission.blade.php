@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
        <h2>Edit Loan Application: {{ $submission->loan->name }}</h2>
            @include('flash::message')  
            <form method="POST" action="{{ url('/my-loans/'. $submission->id .'/update') }}" class="loan-form" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="card">
                    <div class="card-header">Personal Details</div>
                    <div class="card-body">
                        <input type="hidden" name="loan_id" value = "{{ $submission->loan->id }}">
                        <input type="hidden" name="id" value = "{{ $submission->id }}">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">First Name <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="first_name" value="{{ $submission->first_name }}">
                                    @if ($errors->has('first_name'))
                                        <span class="help-block text-danger">
                                            * {{ $errors->first('first_name') }}
                                        </span>
                                    @endif   
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Last Name <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="last_name" value="{{ $submission->last_name }}">
                                    @if ($errors->has('last_name'))
                                        <span class="help-block text-danger">
                                            * {{ $errors->first('last_name') }}
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div><!-- /row -->
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Date of birth</label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1"><i class="fas fa-calendar-alt"></i></span>
                                        </div>
                                        <!-- <input class="datepicker" data-date-format="mm/dd/yyyy"> -->
                                        <input type="text" class="form-control" name="date_of_birth" placeholder="mm / dd / yy" value="{{ $submission->date_of_birth }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="">Contact number <span class="text-danger">*</span></label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1"><i class="fas fa-phone-alt"></i></span>
                                        </div>
                                        <input type="text" class="form-control" name="phone1" value="{{ $submission->phone1 }}">
                                    </div>
                                    @if ($errors->has('phone1'))
                                        <span class="help-block text-danger">
                                            * Contact number is mandatory
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Home address <span class="text-danger">*</span></label>
                                    <textarea name="home_address" class="form-control" cols="20" rows="5">{{ $submission->home_address }}</textarea>
                                </div>
                            </div>
                        </div><!-- /row -->
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">Identity Details</div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="">Social Security Number(SSN) <span class="text-danger">*</span></label>
                                    <input type="text" name="ssn" class="form-control" id="" value="{{ $submission->ssn }}">
                                    @if ($errors->has('ssn'))
                                        <span class="help-block text-danger">
                                            * {{ $errors->first('ssn') }}
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="">Passport number <span class="text-danger">*</span></label>
                                    <input type="text" name="passport_number" class="form-control" id="" value="{{ $submission->passport_number }}">
                                    @if ($errors->has('passport_number'))
                                        <span class="help-block text-danger">
                                            * {{ $errors->first('passport_number') }}
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="">Driver's license</label>
                                    <input type="text" name="drivers_license" class="form-control" id="" value="{{ $submission->drivers_license }}">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">Profession Details</div>
                    <div class="card-body">
                        
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Company name</label>
                                    <input type="text" name="job_name" class="form-control" value="{{ $submission->job_name }}">
                                </div> 
                                <div class="form-group">
                                    <label for="">Net income </label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1"><i class="fas fa-dollar-sign"></i></span>
                                        </div>
                                        <input type="text" class="form-control" name="salary" value="{{ $submission->salary }}">
                                    </div>
                                </div>   
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Company address</label>
                                    <textarea name="job_address" class="form-control" cols="30" rows="5">{{ $submission->job_address }}</textarea>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">Loan Details</div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="">Requested amount <span class="text-danger">*</span></label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1"><i class="fas fa-dollar-sign"></i></span>
                                        </div>
                                        <input type="text" class="form-control" name="request_amount" value="{{ $submission->request_amount }}">
                                    </div>
                                    @if ($errors->has('request_amount'))
                                        <span class="help-block text-danger">
                                            * Loan amount is mandatory
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="">Maturity <span class="text-danger">*</span></label>
                                    <input type="text" name="maturity" class="form-control" id="" value="{{ $submission->maturity }}">
                                    @if ($errors->has('maturity'))
                                        <span class="help-block text-danger">
                                            * Maturity is mandatory
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="">Capacity</label>
                                    <input type="text" name="capacity" class="form-control" id="" value="{{ $submission->capacity }}">
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- /card -->
                <div class="card">
                    <div class="card-header">Supporting Documents</div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="">Bank Statement <span class="text-danger">*</span></label>
                                    <input type="file" name="bank_statement" id="" value=""><br>
                                    @if ($errors->has('bank_statement'))
                                        <span class="help-block text-danger">
                                            * Attach your bank statement
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="">IT Returns <span class="text-danger">*</span></label>
                                    <input type="file" name="it_returns" id="" value=""><br>
                                    @if ($errors->has('it_returns'))
                                        <span class="help-block text-danger">
                                            * Attach your IT returns
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="">Address proof </label>
                                    <input type="file" name="address_proof" id="" value="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- /card -->
                <div class="row">
                    <div class="col-md-6">
                        <button type="reset" class="btn btn-info btn-block text-white text-uppercase">Reset Details</button>
                    </div>
                    <div class="col-md-6">
                        <button type="submit" class="btn btn-block btn-success text-uppercase">Update Loan Application</button>
                    </div>        
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <span class="card-heading">My Loans</span>
                </div>
                <div class="card-body">
                    @include('flash::message')  
                    @if(count($submissions) > 0)
                        <table class="table table-hover">
                            <thead class="thead-light">
                                <tr>
                                <th scope="col">Name</th>
                                <th scope="col">Status</th>
                                <th scope="col">Created On</th>
                                <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($submissions as $submission)    
                                <tr>
                                <td>{{ $submission->loan->name }}</td> 
                                <td>
                                @if($submission->status == 'pending')
                                        <span class="badge badge-primary">
                                    @elseif($submission->status == 'approved')
                                        <span class="badge badge-success">
                                    @elseif($submission->status == 'rejected')
                                        <span class="badge badge-danger">
                                    @endif
                                    {{ $submission->status }}
                                    </span>
                                    </td> 
                                <td><i class="fas fa-calendar-alt"></i> {{ $submission->created_at }}</td> 
                                <td>
                                    <ul class="list-inline list-unstyled">
                                        <li class="list-inline-item"><a href="{{ url('/my-loans/'.$submission->id.'/view') }}" class="btn btn-success text-white btn-sm"><i class="fas fa-eye"></i></a></li>
                                        <li class="list-inline-item"><a href="{{ url('/my-loans/'. $submission->id .'/edit') }}" class="btn btn-info text-white btn-sm"><i class="fas fa-edit"></i></a></li>
                                        <li class="list-inline-item"><a href="{{ url('/my-loans/'. $submission->id .'/delete') }}" class="btn btn-danger btn-sm"><i class="fas fa-trash-alt"></i></a></li>
                                    </ul>
                                </td> 
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        
                    @else
                        <div class="alert alert-warning">
                            Loan applications are empty!
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

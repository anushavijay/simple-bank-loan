@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card view-loan">
                <div class="card-header">
                    <span class="card-heading">{{ $loan->name }}</span>
                    <ul class="list-inline list-unstyled">
                        <li class="list-inline-item"><a href="{{ url('/apply-loan/'.$loan->id) }}" class="btn btn-success text-white btn-sm"><i class="fas fa-hand-holding-usd"></i> Apply for loan</a></li>    
                    </ul>
                </div>
                <div class="card-body">
                    @include('flash::message')  
                    <h4>Purpose</h4>
                    <p>{{ $loan->purpose }}</p>
                    <h4>Eligibility</h4>
                    <p>{{ $loan->eligibility }}</p>
                    <h4>Repayment</h4>
                    <p>{{ $loan->repayment }}</p>
                    <h4>Dcouments Required</h4>
                    <p>{{ $loan->documents }}</p>
                    <p><b>Rate of Interest:</b> {{ $loan->interest }}</p>
                    <p><b>Tenure for loan:</b> {{ $loan->tenure }}</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

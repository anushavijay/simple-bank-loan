@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card view-loan">
                <div class="card-header">
                    <span class="card-heading">Application for {{ $submission->loan->name }} ({{ $submission->status }})</span>
                    <ul class="list-inline list-unstyled">
                        <li class="list-inline-item"><a href="{{ url('/my-loans/'. $submission->id .'/edit') }}" class="btn btn-primary text-white btn-sm"><i class="fas fa-edit"></i> Edit Application</a></li>
                        <li class="list-inline-item"><a href="{{ url('/my-loans/'. $submission->id .'/delete') }}" class="btn btn-danger btn-sm"><i class="fas fa-trash-alt"></i> Delete Application</a></li>
                    </ul>
                </div>
                <div class="card-body">
                    @include('flash::message')  
                    <ul class="list-group">
                        <li class="list-group-item">
                            <h5>Personal Details</h5>
                            <div class="row">
                                <div class="col-md-6"><b>First Name:</b> {{ $submission->first_name }}</div>
                                <div class="col-md-6"><b>Last Name:</b> {{ $submission->last_name }}</div>
                                <div class="col-md-6"><b>Date of Birth:</b> {{ $submission->date_of_birth }}</div>
                                <div class="col-md-6"><b>Contact Number:</b> {{ $submission->phone1 }}</div>
                                <div class="col-md-6"><b>Home Address:</b> {{ $submission->home_address }}</div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <h5>Identity Details</h5>
                            <div class="row">
                                <div class="col-md-4"><b>SSN:</b> {{ $submission->ssn }}</div>
                                <div class="col-md-4"><b>Passport Number:</b> {{ $submission->passport_number }}</div>
                                <div class="col-md-4"><b>Driver's License:</b> {{ $submission->drivers_license }}</div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <h5>Profession Details</h5>
                            @if($submission->job_type == 'employee')
                            <div class="row">
                                <div class="col-md-6"><b>Employer Name:</b> {{ $submission->job_name }}</div>
                                <div class="col-md-6"><b>Net Salary:</b> {{ $submission->job_address }}</div>
                                <div class="col-md-6"><b>Company Address:</b> {{ $submission->salary }}</div>
                            </div>
                            @endif
                            @if($submission->job_type == 'business')
                            <div class="row">
                                <div class="col-md-6"><b>Business Name:</b> {{ $submission->job_name }}</div>
                                <div class="col-md-6"><b>Business Capital:</b> {{ $submission->job_address }}</div>
                                <div class="col-md-6"><b>Business Address:</b> {{ $submission->salary }}</div>
                            </div>
                            @endif
                        </li>
                        <li class="list-group-item">
                            <h5>Loan Details</h5>
                            <div class="row">
                                <div class="col-md-4"><b>Requested Amount:</b> {{ $submission->request_amount }}</div>
                                <div class="col-md-4"><b>Maturity:</b> {{ $submission->maturity }}</div>
                                <div class="col-md-4"><b>Capacity:</b> {{ $submission->capacity }}</div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <h5>Required Documents</h5>
                            <div class="row">
                                <div class="col-md-6"><b>Bank Statment:</b> 
                                @if($submission->bank_statement)  
                                    <a class="text-info" href="{{ Storage::url('documents/' . $submission->bank_statement) }}" download><i class="far fa-file-word"></i> {{ $submission->bank_statement }}</a>
                                @else
                                    <span class="text-danger">No file attached</span>
                                @endif
                                </div>
                                <div class="col-md-6"><b>Address Proof:</b> 
                                @if($submission->address_proof)    
                                    <a class="text-info" href="{{ Storage::url('documents/' . $submission->address_proof) }}" download><i class="far fa-file-word"></i>  {{ $submission->address_proof }} </a>
                                @else
                                    <span class="text-danger">No file attached</span>
                                @endif
                                </div>
                                <div class="col-md-6"><b>It Returns:</b>
                                @if($submission->it_returns)        
                                    <a class="text-info" href="{{ Storage::url('documents/' . $submission->it_returns) }}" download><i class="far fa-file-word"></i> {{ $submission->it_returns }}</a>
                                @else
                                    <span class="text-danger">No file attached</span>
                                @endif
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <h5>Comments</h5>
                                <div class="row">
                                    <div class="col-md-12">
                                    @if($submission->reason)    
                                        {{ $submission->reason }}
                                    @else
                                        <div class="alert alert-warning">
                                            No comments added yet!
                                        </div>
                                    @endif
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

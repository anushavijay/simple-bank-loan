<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>The Lenders</title>

        <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

<!-- Fonts -->
<link rel="dns-prefetch" href="//fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

<!-- Styles -->
<link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    </head>
    <body>
    <nav class="navbar navbar-expand-md navbar-light shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                <img src="{{ url('/logo.png') }}" width="10%"> The Lenders
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link active" href="{{ route('home') }}">Home</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ Request::segment(1) == 'about' ? 'active' : '' }}" href="{{ route('about') }}">About</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ Request::segment(1) == 'contact' ? 'active' : '' }}" href="{{ route('contact') }}">Contact</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ Request::segment(1) == 'login' ? 'active' : '' }}" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link {{ Request::segment(1) == 'register' ? 'active' : '' }}" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @endguest
                        @if(Auth::user())
                            @if(Auth::user()->role == 'admin')
                            <li class="nav-item">
                                <a class="nav-link {{ Request::segment(1) == 'dashboard' ? 'active' : '' }}" href="{{ route('home') }}">Dashboard</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ Request::segment(2) == 'loans' ? 'active' : '' }}" href="{{ route('showAdminLoans') }}">Loans</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ Request::segment(2) == 'submissions' ? 'active' : '' }}" href="{{ route('showAdminSubmissions') }}">Submissions</a>
                            </li>
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                            @endif
                        @endif
                        @if(Auth::user())
                            @if(Auth::user()->role == 'user')
                            <li class="nav-item">
                                <a class="nav-link" href="{{ url('/') }}">Home</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ Request::segment(1) == 'about' ? 'active' : '' }}" href="{{ route('about') }}">About</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ Request::segment(1) == 'contact' ? 'active' : '' }}" href="{{ route('contact') }}">Contact</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ Request::segment(1) == 'submissions' ? 'active' : '' }}" href="{{ route('showSubmissions') }}">My Loans</a>
                            </li>
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                            @endif
                        @endif
                    </ul>
                </div>
            </div>
        </nav>  
        <div class="container">
            
            <h2 class="text-center" style="margin-top:50px;">Welcome to Banking Loan Application</h2>
            <p style="margin-top:20px">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
            @if(count($loans) > 0)
            <div class="row">
            @foreach($loans as $loan) 
                <div class="col-md-3">
                <div class="card">
                    @if($loan->id === 1)
                    <img src="{{ url('/Vehicle.jpg') }}" class="card-img-top" alt="...">
                    @endif
                    @if($loan->id === 2)
                    <img src="{{ url('/Personal.jpg') }}" class="card-img-top" alt="...">
                    @endif
                    @if($loan->id === 3)
                    <img src="{{ url('/House.jpg') }}" class="card-img-top" alt="...">
                    @endif
                    @if($loan->id === 4)
                    <img src="{{ url('/Education.jpg') }}" class="card-img-top" alt="...">
                    @endif
                    @if($loan->id === 5)
                    <img src="{{ url('/Two-wheeler.jpg') }}" class="card-img-top" alt="...">
                    @endif
                    @if($loan->id === 6)
                    <img src="{{ url('/Agriculture.jpg') }}" class="card-img-top" alt="...">
                    @endif
                    @if($loan->id === 9)
                    <img src="{{ url('/Travel.jpg') }}" class="card-img-top" alt="...">
                    @endif
                    @if($loan->id === 10)
                    <img src="{{ url('/Business.jpg') }}" class="card-img-top" alt="...">
                    @endif
                    <div class="card-body">
                        <h5 class="card-title">{{ $loan->name }}</h5>
                        <p class="card-text">Interest: {{ $loan->interest }}% | Tenure: {{ $loan->tenure }}
                        </p>
                        <a href="{{ url('/loans/'.$loan->id) }}" class="btn btn-primary"><i class="fas fa-file-invoice-dollar"></i> Click for more details</a>
                    </div>
                    </div>
                </div>
            @endforeach
            </div>
            @endif
           
        </div>  
    </body>
</html>
<style>
.card-img-top {
    height: 142px;
}
.navbar {
    background: #3490dc;
}
.nav-link, .navbar-brand, .nav-link.active{
    color: #FFF !important;
}
</style>
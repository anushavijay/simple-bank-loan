<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@welcome')->name('home');

Auth::routes();

Route::get('/dashboard', 'HomeController@dashboard')->name('home')->middleware('auth');
Route::get('/about', 'HomeController@about')->name('about');
Route::get('/contact', 'HomeController@contact')->name('contact');
Route::get('/apply-loan/{id}', 'HomeController@createSubmission')->name('createSubmission')->middleware('auth');
Route::get('/loans/{loan}', 'HomeController@viewLoan')->name('viewUserLoan')->middleware('auth');
Route::post('/apply-loan', 'HomeController@storeSubmission')->name('storeSubmission')->middleware('auth');
Route::get('/my-loans', 'HomeController@showSubmissions')->name('showSubmissions')->middleware('auth');
Route::get('/my-loans/{submission}/view', 'HomeController@viewSubmission')->name('viewSubmission')->middleware('auth');
Route::get('/my-loans/{submission}/edit', 'HomeController@editSubmission')->name('editSubmission')->middleware('auth');
Route::post('/my-loans/{id}/update', 'HomeController@updateSubmission')->name('updateSubmission')->middleware('auth');
Route::get('/my-loans/{submission}/delete', 'HomeController@deleteSubmission')->name('deleteSubmission')->middleware('auth');

/** Admin routes */
Route::get('/dashboard/loans', 'AdminController@showLoans')->name('showAdminLoans')->middleware('auth');
Route::get('/dashboard/loans/add', 'AdminController@addLoan')->name('addLoan')->middleware('auth');
Route::post('/dashboard/loans/save', 'AdminController@saveLoan')->name('saveLoan')->middleware('auth');
Route::get('/dashboard/loans/{loan}/view', 'AdminController@viewLoan')->name('viewLoan')->middleware('auth');
Route::get('/dashboard/loans/{loan}/delete', 'AdminController@deleteLoan')->name('deleteLoan')->middleware('auth');
Route::get('/dashboard/loans/{loan}/edit', 'AdminController@editLoan')->name('editLoan')->middleware('auth');
Route::post('/dashboard/loans/{loan}/update', 'AdminController@updateLoan')->name('updateLoan')->middleware('auth');

Route::get('/dashboard/submissions', 'AdminController@showSubmissions')->name('showAdminSubmissions')->middleware('auth');
Route::get('/dashboard/submissions/{submission}/view', 'AdminController@viewSubmission')->name('viewAdminSubmission')->middleware('auth');
Route::post('/dashboard/submissions/{submission}/approve', 'AdminController@approveSubmission')->name('approveAdminSubmission')->middleware('auth');
Route::post('/dashboard/submissions/{submission}/reject', 'AdminController@rejectSubmission')->name('rejectAdminSubmission')->middleware('auth');

Route::get('/image/logo', function ($filename) {
    return Image::make(storage_path().'/logo.png')->response();
});
